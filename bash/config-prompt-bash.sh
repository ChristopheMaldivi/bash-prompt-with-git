source ~/.bash/git-prompt.sh

export GIT_PS1_SHOWCOLORHINTS=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_SHOWDIRTYSTATE=true

export PROMPT_COMMAND='__git_ps1 "\[\033[01;32m\]\u@\h:\[\033[01;34m\]\w\[\033[00m\]" "\\\$ "'

