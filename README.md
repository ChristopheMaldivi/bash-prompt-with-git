# bash-prompt-with-git

Provides a BASH prompt with git repo information + a colored prompt

## Installation

- copy the `bash` directory to `~/.bash` (or a name of your choice, `~/.titi` for instance)
- add to the file `~/.bashrc`: `source ~/.bash/config-prompt-bash.sh` (or `source ~/.titi/config-prompt-bash.sh` if you choose `~/.titi` in the first place)
- you are done, open a new terminal and check the result in a normal directory and in a git repository

## How does it work? how can I update it?

- the `bash/git-prompt.sh` was taken from the [official git repository](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh)
- the `bash/git-completion.bash` file was taken from the [official git repository](https://github.com/git/git/blob/master/contrib/completion/git-completion.bash)

So you can update these files thanks to the official github pages.

Each file provides useful information on how to use/install it, in the comments at the beginning of the file.

## How to configure git prompt?

You can configure the git prompt with supported options (described in the `bash/git-prompt.sh` file).

To change it, open the `~/.bash/config-prompt-bash.sh` file and for instance change this line: 
```
export GIT_PS1_SHOWCOLORHINTS=true
```

## How to configure the prompt colors and misc infos?

Open file `~/.bash/config-prompt-bash.sh`.

You can tune the following line to update colors and info (RTFM ;) ):

```
export PROMPT_COMMAND='__git_ps1 "\[\033[01;32m\]\u@\h:\[\033[01;34m\]\w\[\033[00m\]" "\\\$ "'
```
